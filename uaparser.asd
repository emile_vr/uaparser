;; uaparser - parser for user agent strings, based on common uaparser project
;; E. van Raaij, 26Digits
;;
(asdf:defsystem #:uaparser
  :description "User Agent Parser"
  :version 0.1
  :author "Emile van Raaij <emile@26digits.com>, 26Digits"
  :license "LLGPL"
  :depends-on (#:cl-yaml
               #:cl-ppcre)
  :components ((:module "src"
                :serial t
                :components
                (
                 (:file "uaparser")))))
