## UAPARSER - a User Agent Parser for Common Lisp
A simple CL implementation for the User Agent parser that exists for a number of languages. It extracts browser, operating system, and device information from a useragent string and returns it in a format that is flexible enough for further processing.

## Install & Usage:
1. Copy to your local quicklisp repo folder

2. eval (ql:quickload :uaparser)

3. Get the matcher instance:

    (defparameter *uap* (uaparser:uap-init <path-to-yaml-init-file>))

    uaparser expects a .yaml file with the User Agent data. A version is included, but will not be maintained. You can get it from <https://github.com/ua-parser/uap-core>


4. Simply pass *uap* and the user agent string from your favorite web app to (uaparser:uap-parse ..):

    (uaparser:uap-parse uap "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; fr; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5,gzip(gfe),gzip(gfe)")

    *(((:FAMILY . "Mac OS X") (:MAJOR . "10") (:MINOR . "4") (:PATCH)
    (:PATCH_MINOR))
    ((:FAMILY . "Firefox") (:MAJOR . "3") (:MINOR . "5") (:PATCH . "5"))
    ((:FAMILY . "Other")))*

    (uaparser:uap-parse uap  "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us; Silk/1.1.0-80) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16 Silk-Accelerated=true")

    *(((:FAMILY . "Android") (:MAJOR) (:MINOR) (:PATCH) (:PATCH_MINOR))
     ((:FAMILY . "Amazon Silk") (:MAJOR . "1") (:MINOR . "1") (:PATCH . "0-80"))
     ((:FAMILY . "Kindle") (:BRAND . "Amazon") (:MODEL . "Kindle")))*

5. As shown, the return value will contain a list of assocs with the required data in order os, browser, device


No tests yet.
