(in-package :cl-user)
(defpackage :uaparser
  (:use :cl :cl-ppcre :cl-yaml)
  (:export
   :uap-init
   :uap-parse))
(in-package :uaparser)


(defclass device-matcher ()
  ((pattern_re
     :initarg :pattern_re
     :accessor pattern_re
     :initform (error "Please supply a pattern"))
   (device_replacement
     :initarg :device_replacement
     :accessor device_replacement
     :initform nil)
   (brand_replacement
     :initarg :brand_replacement
     :accessor brand_replacement
     :initform nil)
   (model_replacement
     :initarg :model_replacement
     :accessor model_replacement
     :initform nil)))


(defmethod uas-parse ((matcher device-matcher) &key uas_string)
  (multiple-value-bind (match_string match_elems) (ppcre:scan-to-strings (slot-value matcher 'pattern_re) uas_string)
    (when (not (null match_string))
      (let ((device (slot-value matcher 'device_replacement))
            (brand (slot-value matcher 'brand_replacement))
            (model (slot-value matcher 'model_replacement)))
        ;; TODO: Find out wether in YAML brands and models are given at all. Our current YAML does not define
        ;; anything like that. We leave it in class definition for future implementation.
        (if (not (null device))
            (if (search "$1" device)
                (setf device (ppcre:regex-replace "\\$1" device (elt match_elems 0))))
            (setf device (elt match_elems 0)))
        `((:family . ,device) (:brand . ,brand) (:model . ,model))))))


(defun create-device-matcher (match_instance)
  "return device-instance from yaml"
  (let* ((pattern (gethash "regex" match_instance ""))
         (ignore_case (gethash "regex_flag" match_instance nil))
         (device_r (gethash "device_replacement" match_instance nil))
         (brand_r (gethash "brand_replacement" match_instance nil))
         (model_r (gethash "model_replacement" match_instance nil)))
    (make-instance 'device-matcher
                   :pattern_re (ppcre:create-scanner pattern :single-line-mode t :case-insensitive-mode (string-equal ignore_case "i"))
                   :device_replacement device_r
                   :brand_replacement brand_r
                   :model_replacement model_r)))


(defclass useragent-matcher ()
  ((pattern_re
    :initarg :pattern_re
    :accessor pattern_re
    :initform (error "Please supply a pattern"))
   (family_replacement
    :initarg :family_replacement
    :accessor family_replacement
    :initform nil)
   (v1_replacement
    :initarg :v1_replacement
    :accessor v1_replacement
    :initform nil)
   (v2_replacement
    :initarg :v2_replacement
    :accessor v2_replacement
    :initform nil)
   (v3_replacement
    :initarg :v3_replacement
    :accessor v3_replacement
    :initform nil)))


(defmethod uas-parse ((matcher useragent-matcher) &key uas_string)
  (multiple-value-bind (match_string match_elems) (ppcre:scan-to-strings (slot-value matcher 'pattern_re) uas_string)
    (when (not (null match_string))
      (let ((family (slot-value matcher 'family_replacement))
            (v1 (slot-value matcher 'v1_replacement))
            (v2 (slot-value matcher 'v2_replacement))
            (v3 (slot-value matcher 'v3_replacement))
            (elem_len (length match_elems)))
          (if (> (length family) 0)
            (if (search "$1" family)
                (setf family (ppcre:regex-replace "\\$1" family (elt match_elems 0))))
            (setf family (elt match_elems 0)))
        (if (and (= (length v1) 0) (>= elem_len 2))
            (setf v1 (elt match_elems 1)))
        (if (and (= (length v2) 0) (>= elem_len 3))
            (setf v2 (elt match_elems 2)))
        (if (and (= (length v3) 0) (>= elem_len 4))
            (setf v3 (elt match_elems 3)))
        `((:family . ,family) (:major . ,v1) (:minor . ,v2) (:patch . ,v3))))))


(defun create-useragent-matcher (match_instance)
  "return useragent-instance from yaml"
  (let* ((pattern (gethash "regex" match_instance ""))
         (family_r (gethash "family_replacement" match_instance nil))
         (v1_r (gethash "v1_replacement" match_instance nil))
         (v2_r (gethash "v2_replacement" match_instance nil))
         (v3_r (gethash "v3_replacement" match_instance nil)))
    (make-instance 'useragent-matcher
                   :pattern_re (ppcre:create-scanner pattern :single-line-mode t)
                   :family_replacement family_r
                   :v1_replacement v1_r
                   :v2_replacement v2_r
                   :v3_replacement v3_r)))


(defclass os-matcher ()
  ((pattern_re
    :initarg :pattern_re
    :accessor pattern_re
    :initform (error "Please supply a pattern"))
   (os_replacement
    :initarg :os_replacement
    :accessor os_replacement
    :initform nil)
   (os_v1_replacement
    :initarg :os_v1_replacement
    :accessor os_v1_replacement
    :initform nil)
   (os_v2_replacement
    :initarg :os_v2_replacement
    :accessor os_v2_replacement
    :initform nil)
   (os_v3_replacement
    :initarg :os_v3_replacement
    :accessor os_v3_replacement
    :initform nil)
   (os_v4_replacement
    :initarg :os_v4_replacement
    :accessor os_v4_replacement
    :initform nil)))


(defmethod uas-parse ((matcher os-matcher) &key uas_string)
  (multiple-value-bind (match_string match_elems) (ppcre:scan-to-strings (slot-value matcher 'pattern_re) uas_string)
    (when (not (null match_string))
      (let ((os (slot-value matcher 'os_replacement))
            (os_v1 (slot-value matcher 'os_v1_replacement))
            (os_v2 (slot-value matcher 'os_v2_replacement))
            (os_v3 (slot-value matcher 'os_v3_replacement))
            (os_v4 (slot-value matcher 'os_v4_replacement))
            (elem_len (length match_elems)))
        (if (> (length os) 0)
            (if (search "$1" os)
                (setf os (ppcre:regex-replace "\\$1" os (elt match_elems 0))))
            (setf os (elt match_elems 0)))
        (if (and (= (length os_v1) 0) (>= elem_len 2))
            (setf os_v1 (elt match_elems 1)))
        (if (and (= (length os_v2) 0) (>= elem_len 3))
            (setf os_v2 (elt match_elems 2)))
        (if (and (= (length os_v3) 0) (>= elem_len 4))
            (setf os_v3 (elt match_elems 3)))
        (if (and (= (length os_v4) 0) (>= elem_len 5))
            (setf os_v4 (elt match_elems 4)))
        `((:family . ,os) (:major . ,os_v1) (:minor . ,os_v2) (:patch . ,os_v3) (:patch_minor . ,os_v4))))))


(defun create-os-matcher (match_instance)
  "return os-instance from yaml"
  (let* ((pattern (gethash "regex" match_instance ""))
         (os_r (gethash "os_replacement" match_instance nil))
         (os_v1_r (gethash "os_v1_replacement" match_instance nil))
         (os_v2_r (gethash "os_v2_replacement" match_instance nil))
         (os_v3_r (gethash "os_v3_replacement" match_instance nil))
         (os_v4_r (gethash "os_v4_replacement" match_instance nil)))
    (make-instance 'os-matcher
                   :pattern_re (ppcre:create-scanner pattern :single-line-mode t)
                   :os_replacement os_r
                   :os_v1_replacement os_v1_r
                   :os_v2_replacement os_v2_r
                   :os_v3_replacement os_v3_r
                   :os_v4_replacement os_v4_r)))


(defun build-matchers (yaml_instances build_fnc)
  "create a list of matchers for instance"
  (mapcar #'(lambda (yaml_instance)
              (funcall build_fnc yaml_instance)) yaml_instances))


(defun extract-yaml (yaml_path_name)
  "extract yaml data to internal formatted sections"
  (let* ((parsers (yaml:parse yaml_path_name))
         (os_parsers (gethash "os_parsers" parsers))
         (device_parsers (gethash "device_parsers" parsers))
         (user_agent_parsers (gethash "user_agent_parsers" parsers)))
    (values os_parsers device_parsers user_agent_parsers)))


(defun run-parser (match_collection user_agent_string)
  "given the list of matchers, parse using the first correct one or return nil"
  (let ((data nil))
    (dolist (matcher match_collection)
      (setf data (uas-parse matcher :uas_string user_agent_string))
      (if (not (null data))
          (return)))
    data))


(defun or-other (ua_element)
  "return default when ua-element nil"
  (if (null ua_element)
      `((:family . "Other"))
      ua_element))


;; public accessibles from here
(defun uap-init (yaml_path_name)
  "build more efficient matcher based on yaml"
  (multiple-value-bind (os_r dev_r ua_r) (extract-yaml (pathname yaml_path_name))
    (let ((os_m (build-matchers os_r #'create-os-matcher))
          (ua_m (build-matchers ua_r #'create-useragent-matcher))
          (dev_m (build-matchers dev_r #'create-device-matcher)))
      `((:os_matchers . ,os_m) (:ua_matchers . ,ua_m) (:device_matchers . ,dev_m)))))


(defun uap-parse (assoc_matchers user_agent_string)
  "collect all data from user agent string and return as parsable structure"
  (let ((os_info (run-parser (cdr (assoc :os_matchers assoc_matchers)) user_agent_string))
        (ua_info (run-parser (cdr (assoc :ua_matchers assoc_matchers)) user_agent_string))
        (device_info (run-parser (cdr (assoc :device_matchers assoc_matchers)) user_agent_string)))
    (list (or-other os_info) (or-other ua_info) (or-other device_info))))
